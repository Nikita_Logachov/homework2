package com.company;

import java.text.DecimalFormat;

public class Main {

    public static void main(String[] args) {
        //1) В переменных q и w хранятся два натуральных числа. Создайте программу, выводящую на экран результат деления q на w с остатком.
        System.out.println("1) В переменных q и w хранятся два натуральных числа. Создайте программу, выводящую на экран результат деления q на w с остатком.");
        int q = 0 + (int) (Math.random() * 100);
        int w;
        do {
            w = 0 + (int) (Math.random() * 100);
        } while (w > q);
        System.out.println("q / w = " + q + " / " + w + " = " + q / w + ", остаток " + q % w);
        System.out.println();

        //2) В переменной n хранится натуральное двузначное число. Создайте программу, вычисляющую и выводящую на экран сумму цифр числа n.
        System.out.println("2) В переменной n хранится натуральное двузначное число. Создайте программу, вычисляющую и выводящую на экран сумму цифр числа n.");
        int n = 10 + (int) (Math.random() * 99);
        System.out.println("Число n = " + n + "; Сумма цифр = " + SumOfNum(n));
        System.out.println();

        //3) В переменной n хранится вещественное число с ненулевой дробной частью. Создайте программу, округляющую число n до ближайшего целого и выводящую результат на экран.
        System.out.println("3) В переменной N хранится вещественное число с ненулевой дробной частью. Создайте программу, округляющую число N до ближайшего целого и выводящую результат на экран.");
        double N = 10 + Math.random() * (99 - 10);
        System.out.println("Дробное N = " + N + "; Натуральное N = " + Math.round(N));
        System.out.println();

        //4) В переменной n хранится натуральное трёхзначное число. Создайте программу, вычисляющую и выводящую на экран сумму цифр числа n.
        System.out.println("4) В переменной n хранится натуральное трёхзначное число. Создайте программу, вычисляющую и выводящую на экран сумму цифр числа n.");
        n = 100 + (int) (Math.random() * 999);
        System.out.println("Число n = " + n + "; Сумма цифр = " + SumOfNum(n));
        System.out.println();

        //5) Допустим, результат выполнения программы выглядит следующим образом:
        //    One
        //    Two
        //    Three
        //   Напишите строку кода с вызовом метода println (), где этот результат выводится в одной символьной строке.
        System.out.println("5) Напишите строку кода с вызовом метода println (), где этот результат выводится в одной символьной строке.");
        System.out.println(" One\n Two\n Three\n");

        //6) В переменной n хранится вещественное трёхзначное число с 2-мя знаками после запятой. Создайте программу, вычисляющую и выводящую на экран сумму всех цифр числа n.
        System.out.println("6) В переменной N хранится вещественное трёхзначное число с 2-мя знаками после запятой. Создайте программу, вычисляющую и выводящую на экран сумму всех цифр числа N.");
        N = 100 + Math.random() * (999 - 100);
        N = Math.round(N * 100.0) / 100.0;
        System.out.println("Число N = " + N + "; Сумма цифр = " + SumOfNum(N));
        System.out.println();

        //7) В переменной n хранится вещественное трёхзначное число с 2-мя знаками после запятой. Создайте программу, вычисляющую и выводящую на экран max и min цифру числа n.
        System.out.println("7) В переменной n хранится вещественное трёхзначное число с 2-мя знаками после запятой. Создайте программу, вычисляющую и выводящую на экран max и min цифру числа n.");
        N = 100 + Math.random() * (999 - 100);
        N = Math.round(N * 100.00) / 100.00;
        FindMinMax(N);
        System.out.println();

        //8) В переменной n хранится вещественное трёхзначное число с 5-мя знаками после запятой. Создайте программу, вычисляющую и выводящую на экран значение формулы: цифра сотен + цифра 10-ов - цифра единиц + цифра десятых - цифра сотых и т.д
        System.out.println("8) В переменной n хранится вещественное трёхзначное число с 5-мя знаками после запятой. Создайте программу, вычисляющую и выводящую на экран значение формулы: цифра сотен + цифра 10-ов - цифра единиц + цифра десятых - цифра сотых и т.д");
        N = 100 + Math.random() * (999 - 100);
        String formattedDouble = new DecimalFormat("#0.00000").format(N);
        System.out.println("Число N = " + formattedDouble + "; Сумма цифр = " + Formula(formattedDouble));
        System.out.println();

        //9) Напишите программу, печатающую количество нулевых элементов в заданном целочисленном массиве.
        System.out.println("9) Напишите программу, печатающую количество нулевых элементов в заданном целочисленном массиве.");
        int[] arr = new int[40];
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            arr[i] = 0 + (int) (Math.random() * 10);
            System.out.print(arr[i] + " ");
            if (arr[i] == 0)
                count++;
        }
        System.out.println("\nКол-во 0 = " + count);
        System.out.println();

        //10) Напишите программу, печатающую максимальный элемент непустого массива.
        System.out.println("10) Напишите программу, печатающую максимальный элемент непустого массива.");
        int max = 0;
        for (int i = 0; i < arr.length; i++) {
            arr[i] = 0 + (int) (Math.random() * 100);
            System.out.print(arr[i] + " ");
            if (arr[i] > max)
                max = arr[i];
        }
        System.out.println("\nMAX = " + max);
        System.out.println();

        //11) Создать двумерный массив из 8 строк по 5 столбцов в каждой из случайных целых чисел из отрезка [10;99]. Вывести массив на экран.
        System.out.println("11) Создать двумерный массив из 8 строк по 5 столбцов в каждой из случайных целых чисел из отрезка [10;99]. Вывести массив на экран.");
        int[][] array = new int[8][5];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                array[i][j] = 10 + (int) (Math.random() * 90);
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();

        //12) Электронные часы показывают время в формате от 00:00 до 23:59. Подсчитать сколько раз за сутки случается так, что слева от двоеточия показывается симметричная комбинация для той, что справа от двоеточия (например, 02:20, 11:11 или 15:51).
        System.out.println("12) Электронные часы показывают время в формате от 00:00 до 23:59. Подсчитать сколько раз за сутки случается так, что слева от двоеточия показывается симметричная комбинация для той, что справа от двоеточия (например, 02:20, 11:11 или 15:51).");
        int[] clock = new int[]{0, 0, 0, 0};
        count = 0;
        for (int i = 0; i < 1440; i++) {
            clock[3] = i % 10;
            clock[2] = (i / 10) % 6;
            clock[1] = (i / 60) % 10;
            clock[0] = i / 600;
            if (clock[0] == clock[3] && clock[1] == clock[2]) {
                System.out.println(clock[0] + "" + clock[1] + ":" + clock[2] + "" + clock[3]);
                count++;
            }
        }
        System.out.println(count);

        //13) Напишите программу, печатающую количество максимальных элементов непустого массива, в которой используется только один цикл.
        System.out.println("Напишите программу, печатающую количество максимальных элементов непустого массива, в которой используется только один цикл.");
        count = 0;
        max = 0;
        for (int i = 0; i < arr.length; i++) {
            arr[i] = 0 + (int) (Math.random() * 10);
            System.out.print(arr[i] + " ");
            if (arr[i] > max) {
                max = arr[i];
                count = 1;
            } else if (arr[i] == max)
                count++;
        }
        System.out.println("\nMAX = " + max + "\nКол-во MAX = " + count);
        System.out.println();

        //14) Напишите программу, печатает массив, затем инвертирует (то есть меняет местами первый элемент с последним, второй — с предпоследним и т.д.), и вновь печатает.
        System.out.println("Напишите программу, печатает массив, затем инвертирует (то есть меняет местами первый элемент с последним, второй — с предпоследним и т.д.), и вновь печатает.");
        int[] arr2 = new int[11];
        for (int i = 0; i < arr2.length; i++) {
            arr2[i] = 0 + (int) (Math.random() * 10);
            System.out.print(arr2[i] + " ");
        }
        System.out.println();
        for (int i = 0; i < arr2.length / 2; i++) {
            int tmp = arr2[i];
            arr2[i] = arr2[arr2.length - i - 1];
            arr2[arr2.length - i - 1] = tmp;
        }
        for (int i = 0; i < arr2.length; i++) {
            System.out.print(arr2[i] + " ");
        }
        System.out.println();

        //15) Создать двумерный массив из случайных чисел так чтобы размер внутреннего массива был случайным. Вывести массив на экран.
        System.out.println("Создать двумерный массив из случайных чисел так чтобы размер внутреннего массива был случайным. Вывести массив на экран.");
        int[][] array2 = new int[5][3 + (int) (Math.random() * 10)];
        for (int i = 0; i < array2.length; i++) {
            for (int j = 0; j < array2[0].length; j++) {
                array2[i][j] = 0 + (int) (Math.random() * 10);
                System.out.print(array2[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();

        //16) Выведите на экран первые 11 членов последовательности Фибоначчи. Напоминаем, что первый и второй члены последовательности равны единицам, а каждый следующий — сумме двух предыдущих.
        System.out.println("Выведите на экран первые 11 членов последовательности Фибоначчи. Напоминаем, что первый и второй члены последовательности равны единицам, а каждый следующий — сумме двух предыдущих.");
        int fib0 = 1;
        int fib1 = 1;
        int fib2;
        System.out.print(fib0 + " " + fib1 + " ");
        for (int i = 2; i < 11; i++) {
            fib2 = fib0 + fib1;
            System.out.print(fib2 + " ");
            fib0 = fib1;
            fib1 = fib2;
        }
        System.out.println();

        //17) С помощью программы подсчитайте сколько счастливых билетов в одном рулоне (рулон билетов с номерами от 000001 до 999999)?
        System.out.println("С помощью программы подсчитайте сколько счастливых билетов в одном рулоне (рулон билетов с номерами от 000001 до 999999)?");
        int[] tickets = new int[6];
        count=0;
        for (int i = 0; i < 999999; i++) {
            tickets[5] = i % 10;
            tickets[4] = (i/10) % 10;
            tickets[3] = (i/100) % 10;
            tickets[2] = (i/1000) % 10;
            tickets[1] = (i/10000) % 10;
            tickets[0] = i/100000;
            if(tickets[0]+tickets[1]+tickets[2]==tickets[3]+tickets[4]+tickets[5])
                count++;
        }
        System.out.println("Количество счастливых билетов = " + count);
        System.out.println();
    }

    static int SumOfNum(int number) {
        String temp = Integer.toString(number);
        int[] newNum = new int[temp.length()];
        int result = 0;
        for (int i = 0; i < temp.length(); i++) {
            newNum[i] = temp.charAt(i) - '0';
            result += newNum[i];
        }
        return result;
    }

    static int SumOfNum(double number) {
        String temp = Double.toString(number);
        int[] newNum = new int[temp.length() - 1];
        int result = 0;
        int k = 0;
        for (int i = 0; i < temp.length(); i++) {
            if (temp.charAt(i) != '.') {
                newNum[k] = temp.charAt(i) - '0';
                result += newNum[k];
                k++;
            }
        }
        return result;
    }

    static void FindMinMax(double number) {
        String temp = Double.toString(number);
        int[] newNum = new int[temp.length() - 1];
        int k = 0;
        int min = 9, max = 0;
        for (int i = 0; i < temp.length(); i++) {
            if (temp.charAt(i) != '.') {
                newNum[k] = temp.charAt(i) - '0';
                if (min > newNum[k])
                    min = newNum[k];
                if (max < newNum[k])
                    max = newNum[k];
                k++;
            }
        }
        System.out.println("Число N = " + number + "; min = " + min + "; max = " + max);
    }

    static int Formula(String temp) {
        int[] newNum = new int[temp.length() - 1];
        int result = 0;
        int k = 0;
        for (int i = 0; i < temp.length(); i++) {
            if (temp.charAt(i) != ',') {
                newNum[k] = temp.charAt(i) - '0';
                if (k > 1)
                    result += newNum[k] * Math.pow((-1), k - 1);
                else
                    result += newNum[k];
                k++;
            }
        }
        return result;
    }
}
